import React, {Component} from 'react';
import ReactDOM from 'react-dom';

export default class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            input: '',
            name: ''
        };
    }

    render() {
        return (
            <div>
            <input placeholder="Enter your name" onChange={event => this.setState({input: event.target.value})} />
            Your name is {this.state.input}
            <br />
            <button type="button" className="btn btn-primary" onClick={() => {this.setState({name: this.state.input})}}>Enter</button>
            <br />
            {getGreeting(this.state.name)}
            <input onChange={event => {this.props.handler(event)}} />
            <button type="button" className="btn btn-primary">Search</button>
            
            </div>
        );

        function getGreeting(user) {
            if (user)
              return <p>Hello, {user}!</p>;
          }
    }
}