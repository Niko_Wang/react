var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var config = require('./webpack.config');

new WebpackDevServer(webpack(config), {
  contentBase: "public",
  publicPath: config.output.publicPath,
  hot: true,
  historyApiFallback: true,
  proxy: {
    "/api/*": {
      target: "http://localhost:8080"
    }
  }
}).listen(3000, '0.0.0.0', function (err, result) {
  if (err) {
    return console.log(err);
  }

  console.log('Listening at http://localhost:3000/');
});

var http = require("https");
var express = require('express');

var app = express();
var server = app.listen(8000, function() {
    console.log('Server is running...');
});

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', "*");
  res.header('Access-Control-Allow-Methods','GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
})


app.get('/:word', sendData);
function sendData(request, response) {
  var self = response;
  var options = {
    host: 'www.googleapis.com',
    path: '/youtube/v3/search?part=snippet&q='+ request.params.word +'&key=AIzaSyBiwf2yormkbBg-GSEbyCCtCopXP01Ax1s',
    method: 'GET'
  };
  

  var req = http.request(options,function(response) {
    var body = '';
    response.on('data', function(chunk) {
        body += chunk;
      });
      response.on('end', function() {
        self.send(JSON.parse(body));
      })
        //self.send(JSON.parse(chunk));
    
})
  req.end();

}