import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import Search from './search.js';
import VideoList from './video_list.js';

export default class Root extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = { 
            videos: [],
            searchinput: '',
            handler: function(event) {
                this.setState({searchinput: event.target.value})
            }
         };
         this.handler = this.state.handler.bind(this);
    }

    componentWillMount() {
        fetch('http://localhost:8000/Mac')
        .then(results => {
            //console.log(results.json());
            return results.json();
        }).then(data => {
            this.setState({videos: data.items});
            
        });
    }

    render() {
        if (!this.state.videos) return <p>Loading...</p>
        return(
            <div>
                {this.state.searchinput}
                <Search handler={this.handler} />
                <VideoList videos={this.state.videos} />
            </div>
        )
    }
}